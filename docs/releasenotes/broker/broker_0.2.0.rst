CI Token Broker v0.2.0
=======================

* *Release*: `v0.2.0 <https://gitlab.com/ecp-ci/ci-token-broker/-/releases/v0.2.0>`_
* *Date*: 3/24/2021

.. Warning::

    The CI Token Broker is not meant for production systems at this time and
    should only be used in development/test environment to inspect the
    validity of the workflow.

Admin Changes
-------------

* Optional ``--insecure`` flag allows use of HTTP mode for testing
  purposes only
  (`!15 <https://gitlab.com/ecp-ci/ci-token-broker/-/merge_requests/15>`_)

* Gracefully handle server shutdown
  (`!16 <https://gitlab.com/ecp-ci/ci-token-broker/-/merge_requests/16>`_)

* Docker environment to support development/testing environments
  (`!19 <https://gitlab.com/ecp-ci/ci-token-broker/-/merge_requests/19>`_,
  `!24 <https://gitlab.com/ecp-ci/ci-token-broker/-/merge_requests/24>`_)


Bug & Development Fixes
-----------------------

* Jacamar CI ``pkg/gitlabjwt`` is used as opposed to previously managed
  internal packages
  (`!20 <https://gitlab.com/ecp-ci/ci-token-broker/-/merge_requests/20>`_)

  - This will align a single managed/audited method for validating a
    ``CI_JOB_JWT`` against a GitLab server.

* Updated job registration body and response to allow additional flexibility and
  provide with the correct broker token
  (`!17 <https://gitlab.com/ecp-ci/ci-token-broker/-/merge_requests/17>`_)

  - .. code-block:: javascript
       :caption: Example Request

        {
           "ci_job_jwt": "...",
           "ci_job_token": "...",
           "service_user": "..."
        }

  - .. code-block:: javascript
       :caption: Example Response (HTTP/1.1 201 CREATED)

        {
           "broker_token": "..."
        }

* Expand ``web`` package testing
  (`!21 <https://gitlab.com/ecp-ci/ci-token-broker/-/merge_requests/21>`_)

  - This introduces a ``tools/mock-gl-api`` to provide mock request/responses
    that are easier to manage than a traditional ``gomock`` would offer.
