HPC GitLab-Runner 12.7.0~hpc.0.6.1
==================================

* *Release Date*: 3/26/2020
* *Commit*: 8e627e41
* *Tag*: `v12.7.0-hpc.0.6.1 <https://gitlab.com/ecp-ci/gitlab-runner/-/tags/v12.7.0-hpc.0.6.1>`_

Admin Changes
-------------

* Added configuration option to allow a administrator defined path to be
  observed used as a prefix for all job submission commands.

  .. code-block:: toml

    [[runners]]
      scheduler_bin = "/usr/scheduler/bin"

  - Using the above example with LFS, the job submission command executed
    by the user will be ``$ /usr/scheduler/bin/bsub ...``.

  - The goal of this change is to provide an experimental option that can allow
    for site deployments that may require a wrapper around the traditional
    job submission application.
