:orphan:

All Releases
============

Detailed release notes for all runner and server development efforts. Due to
the nature of managing multiple forks as well as distinct HPC focused libraries
centralizing notes hopefully allows for easier review as well as
clearer documentation of the changes for both administrators and users alike.

.. toctree::
   :maxdepth: 1
   :caption: Jacamar CI

   jacamar/jacamar_0.7.3.rst
   jacamar/jacamar_0.7.2.rst
   jacamar/jacamar_0.7.1.rst
   jacamar/jacamar_0.7.0.rst
   jacamar/jacamar_0.6.0.rst
   jacamar/jacamar_0.5.0.rst
   jacamar/jacamar_0.4.2.rst
   jacamar/jacamar_0.4.1.rst
   jacamar/jacamar_0.4.0.rst
   jacamar/jacamar_0.3.2.rst
   jacamar/jacamar_0.3.1.rst
   jacamar/jacamar_0.3.0.rst
   jacamar/jacamar_0.2.0.rst
   jacamar/jacamar_0.1.0.rst

.. toctree::
   :maxdepth: 1
   :caption: CI Token Broker

   broker/broker_0.3.0.rst
   broker/broker_0.2.0.rst

.. important::

   All further development efforts have been shifted to supporting
   our implementation of a
   `custom executor driver (Jacamar CI) <https://gitlab.com/ecp-ci/jacamar-ci>`_,
   as opposed of further enhancing
   the complete `fork <https://gitlbab.com/ecp-ci/gitlab-runner>`_
   of the upstream runner. We are committed to continuing support
   for all ECP enhancements in this greatly improved CI model. All feedback
   is welcome as we continue to work towards a production ready release.

.. toctree::
   :maxdepth: 1
   :caption: Runner (fork w/ECP Enhancements)

   runner/runner_13.0.1_0.6.1.rst
   runner/runner_13.0.0_0.6.1.rst
   runner/runner_12.7.0_0.6.1.rst
   runner/runner_12.7.0_0.6.0.rst
   runner/runner_12.6.0_0.5.4.rst
   runner/runner_12.6.0_0.5.3.rst
   runner/runner_12.4.0_0.5.2.rst
   runner/runner_12.4.0_0.5.1.rst
   runner/runner_12.4.0_0.5.0.rst
   runner/runner_12.0.0_0.4.3.rst
