Project Developer
=================

.. _enhancements: ../index.html#enhancements
.. _administrator documentation: admin.html
.. _official documentation: https://docs.gitlab.com

Documentation, best practices, and tutorials that are focused on making the
most out of HPC resources via ECP CI enhancements. As such, information
presented here is centred on those HPC focused enhancements_ made to
to GitLab. We will make ample reference to GitLab's `official documentation`_
and strongly encourage you to look there for details not covered in these
pages. For any questions on runner or server deployment please see the
`administrator documentation`_.

Continuous Integration
----------------------

Understanding the `GitLab Runner <https://gitlab.com/gitlab-org/gitlab-runner>`_
is the key to making full use of the GitLab CI ecosystem. These sections
are targeted not just at those unfamiliar with using the GitLab runner but
anyone who wishes to understand how the ECP CI enhancements_ will
affect their CI jobs.

.. toctree::
   :maxdepth: 2

   ci-users/ci-general.rst
   ci-users/ci-batch.rst
   ci-users/ci-faq.rst

Guides
------

Guides have been written to highlight both best practices as well as
potential workflows you may want to leverage when using facility
resources with ECP CI.

.. toctree::
   :maxdepth: 1

   guides/manual-directory-cleanup.rst
   guides/build-status-gitlab.rst
   guides/multi-gitlab-project.rst
   guides/hpc-artifacts-caching.rst

Tutorials
---------

.. list-table::
    :header-rows: 1
    :widths: 15, 5, 5, 5, 5, 20

    * - Title
      - ALCF
      - OLCF
      - NERSC
      - GitLab.com
      - Description
    * - `ECP CI Startup Tutorial <tutorial/startup_tutorial.html>`_
      - √
      - x
      - √
      - √
      - Comprehensive beginner level introduction to CI and ECP supported resources.
    * - `MPI Quick Start <tutorial/quickstart.html>`_
      - √
      - x
      - x
      - x
      - Building and running a basic MPI application across compute resources.
    * - `2020 ECP Annual <tutorial/ecp_ci_startup.html>`_
      - x
      - x
      - x
      - x
      - Provided at the 2020 ECP Annual (**Deprecated**, please only use for reference).

ECP CI tutorials aim to provide a look at how one might use traditional
GitLab functionally coupled with the ECP CI enhancements and available
test resources in a self-paced manner. If your unsure how to get started
using these tutorials this will provide immense value.

.. toctree::
   :maxdepth: 1
   :hidden:

   tutorial/startup_tutorial.rst
   tutorial/quickstart.rst
   tutorial/ecp_ci_startup.rst

Examples
--------

Example projects have been created and verified using the ECP CI enhancements
to provide not only recommended practices but also an interactive avenue to
better understand GitLab CI. Even if you already have experience with the
tools we've taken the time to also highlight HPC examples that can assist you.

.. list-table::
    :header-rows: 1
    :widths: 20, 4, 4, 4, 4, 40

    * - Title
      - ALCF
      - OLCF
      - NERSC
      - GitLab.com
      - Description
    * - `GitLab CI Basics <examples/gitlab-ci-basics.html>`_
      - √
      - √
      - x
      - √
      - A simple multi-stage "Hello World" build/test pipeline.
    * - `Multi-Platform Extends <examples/multi-platform-spack.html>`_
      - √
      - x
      - x
      - x
      - Basic demonstration running across multiple platforms/architectures.

.. toctree::
   :maxdepth: 1
   :hidden:

   examples/gitlab-ci-basics.rst
   examples/multi-platform-spack.rst
