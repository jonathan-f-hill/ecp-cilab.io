CI Token Broker Administration
==============================

.. _Jacamar: ../admin.html#jacamar-ci

.. important::

    The `CI Token Broker <https://gitlab.com/ecp-ci/ci-token-broker>`_
    is under active development and subject to change.
    We **strongly** advise that you only deploy in a testing
    capacity until the official 1.0 release.

GitLab’s
`job permission model <https://docs.gitlab.com/ee/user/project/new_ci_build_permissions_model.html#new-ci-job-permissions-model>`_
enforces that for each job spawned a limited life job token is generated,
this provides select permissions as the CI trigger user to GitLab resources.
For the majority of ECP CI and traditional executors this structure is
incredibly beneficial. However, in the very specific case of the
`RunAs <../jacamar/auth.html#runas-user>`_ (job execution under a shared
local service account) concerns arise with the unfettered access
the service account has to tokens of varying scopes.

The CI Token Broker is the solution to this issue by providing a
specialized web service to manage and enforce token access during
CI jobs when a service account model is being leveraged. This service
acts as proxy that can safely store sensitive tokens that are registered
and respond with a broker only access token. In turn this broker token can
be safely given to users to access a limited subset of resources tied
strictly to the service account's permission on the GitLab instance.

Security Model
--------------

Though the broker is fundamentally a proxy service that is designed to
regulate what can be accessed with CI job tokens (i.e. ``CI_JOB_TOKEN``).
The actual security model surrounding this process can appear
complicated as it requires controls over a number of key operations.

.. image:: files/broker_ci_context.png
    :scale: 60%

1. Since the privileged ``jacamar-auth`` application is already a trusted
   process it will be the gatekeeper for all GitLab generated tokens.
   When `configured <../jacamar/configuration.html#auth-broker-table>`_
   to interact with the broker service it will leverage
   knowledge of job tokens to parse the runner generated script and
   restructure accordingly.

  * It is important to note that the runner itself will have no understanding
    of the service user (RunAs_ feature). As such, Jacamar will need to take
    on responsibility over altering an already generated script. It is only
    through this job script that local users are given access to
    CI variables.

2. ``jacamar-auth`` is also responsible for job registration with the broker
   service. This entails informing the broker of a valid ``CI_JOB_TOKEN``
   as well as ``CI_JOB_JWT`` and target service account for RunAs. Once
   registration is successful the broker will provide it's own token
   that can then be used in lue of the GitLab generated ones.

  * The entire process of registration is handled by an instance of
    Jacamar configured_
  * Registration is tied to the ``CI_JOB_JWT`` whose validity can be
    checked against a GitLab server's
    `JWKS <https://gitlab.com/-/jwks>`_ endpoint.

3. The user's job script execution will proceed as normal; however, since
   the broker URL and JWT have been introduced to the script, requests to
   the broker will happen without the CI user's input.

  * The goal is to simply introduce the broker as a required security
    measure without the user needing to understand the specifics. In the
    same vein it can be removed without the user ever knowing.

4. Since the broker is a proxy it will have
   substantial control over HTTPS requests it receives. Before these can be
   forwarded onto the GitLab instance it will need to ensure that the request
   is valid in a service account model.

   * If the service user associated with the registered broker token
     has been identified with necessary access only then will the GitLab
     job token be substituted in the request and forwarded.
   * The broker will need to maintain an administrative token to the GitLab
     instance to have full access to read required API endpoints.
   * The broker is cable and will often forward request to the GitLab instance
     completely unmodified. In these cases GitLab is capable of rejecting
     all invalid requests as it normally would.

Once complete the workflow will ensure that the GitLab generated
``CI_JOB_TOKEN`` and ``CI_JOB_JWT`` are removed from the user's environment.
Due to the functionality of the broker it is still possible
to preform supported actions with the new broker token with an added layer
of rules enforced by the service.

In our example the token, as seen used in the ``CI_REPOSITORY_URL`` has been
replaced by the broker token. Though since the Git request is made to the
broker it properly resolves:

.. image:: files/broker_job_example.png
    :scale: 75%

Please note that in the above image the newly create broker token is not
masked, unlike what you would expect to see with other token. This is only
present in the development instance and will be removed in production
deployments.

Reliance on Jacamar CI
~~~~~~~~~~~~~~~~~~~~~~

The crucial functionality of the broker relies upon exerting control both
over the environment as well as commands executed as part of the CI job.
To this end it is important to highlight that each stage within a CI job
is ultimately a Bash script that is executed by the CI user.

.. code-block:: bash

    #!/usr/bin/env bash

    set -eo pipefail
    set +o noclobber
    : | eval $'export ...'
    exit 0

Due to this requirement and the unique functionality of Jacamar_ it
has become a crucial piece in the broker's workflow. The privileged
user (via the `jacamar-auth` application) is able to configure broker support
and by doing so the application will not just manage registration with
the service but also replace sensitive tokens in advanced of providing
them to the user.

GitLab Server Accounts
~~~~~~~~~~~~~~~~~~~~~~

Highlighted in the `Security Model`_, the broker supports restricting
token access in the RunAs_ model to that of a service account.
This target service account is declared to the broker during job
registration and in turn the broker will validate that regardless of
the request this service account has some level of access to the
required projects.

As such there must exist a relationship between a service account
on the machine and that of a GitLab account who has been granted
`guest` privileges to the required projects/groups.

.. note::

    Access level is only required for write operations or read-only
    access on `internal`/`private` repositories. Any `public` repo
    will be accessible regardless of user access with the
    provided token.

Deployment
----------

The broker is a single binary written with
`Go <golang.org/>`_ and can be deployed relatively easily with
only a key few requirements:

1. Must be ran in parallel and in the same security envelope as the
   target GitLab server version 13.0+

   * During the Configuration_ you will be required to provide a
     token scoped for API access, it is recommend that this is
     administrative level.

2. Runners must use the custom executor driver Jacamar
   CI `version 0.4.1+ <https://gitlab.com/ecp-ci/jacamar-ci/-/releases>`_

   * Jacamar is only required on the runner host in order to leverage
     the broker service. It is not strictly required when deploying
     on the server.

Once requirements are met you can install the broker application
in one of several supported workflows:

* Build/Install from source:

  * .. code-block:: console

        git clone https://gitlab.com/ecp-ci/ci-token-broker.git
        cd ci-token-broker
        make build
        make install PREFIX=/usr/local

* Deploy using official release RPM:

  * Download latest `release <https://gitlab.com/ecp-ci/ci-token-broker/-/releases>`_
  * ``rpm -Uvh ci-token-broker-*.rpm``

After successfully deploying, the service can be started from command line
(note you will be required to supply a Configuration_ of your own):

.. code-block:: console

    ci-token-broker --config /opt/ci-token-broker/localhost_conf.toml
    [GIN-debug] [WARNING] Running in "debug" mode. Switch to "release" mode in production.
        - using env:	export GIN_MODE=release
        - using code:	gin.SetMode(gin.ReleaseMode)

    [GIN-debug] POST   /-/broker/job             --> gitlab.com/ecp-ci/ci-token-broker/internal/web.(*data).registerJob-fm (4 handlers)

You can refer to ``ci-token-broker --help`` for additional
commands/information.

.. note::

    At this time you are responsible for launching the broker either via
    command line or with the assistance of an operating system service.

Configuration
-------------

In order to run the CI Token Broker you must provide a configuration file
(via ``--config`` flag) at startup. It is required adhere to all
`TOML <https://toml.io/en/>`_ specifications:

.. code-block:: toml

    [general]
      port = "8080"
      gitlab_token = "T0k3n"
      gitlab_url = "https://gitlab.example.com"
      cert_file = ".dev/out/localhost.crt"
      key_file = ".dev/out/localhost.key"
      timeout = "10s"

.. list-table::
    :widths: 10 5 50
    :header-rows: 1

    * - Key
      - Required
      - Description
    * - ``port``
      - X
      - The port on which the server will listen on (default: ``8080``).
    * - ``gitlab_token``
      - √
      - GitLab token scoped for API access, we advise that this token is administrator level to avoid any permission issue with all potential CI jobs.
    * - ``gitlab_url``
      - √
      - URL for the supported GitLab server.
    * - ``cert_file``
      - √*
      - TLS certificate (``.crt``) file.
    * - ``key_file``
      - √*
      - TLS key (``.key``) file.
    * - ``timeout``
      - X
      - HTTP timeout for client requests set by `duration string <https://golang.org/pkg/time/#ParseDuration>`_ (default: ``10s``).

* `√*` - Files for a TLS connection are only required when running the
  broker without declaring the ``--insecure`` flag at startup.
