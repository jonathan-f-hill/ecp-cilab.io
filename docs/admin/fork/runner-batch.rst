:orphan:

Batch Executor
==============

.. _Runner Administration: runner-admin.html
.. _setuid: runner-admin.html#setuid

.. important::

   All further development efforts have been migrated to supporting
   our implementation of a
   `custom executor driver (Jacamar CI) <https://gitlab.com/ecp-ci/jacamar-ci>`_,
   support for the complete `fork <https://gitlbab.com/ecp-ci/gitlab-runner>`_
   has been discontinued. We are committed to continuing support
   for all ECP enhancements in this greatly improved CI model. All feedback
   is welcome as we continue to work towards a production ready release.

The official GitLab Runner already supports a number of
`executors <https://docs.gitlab.com/runner/executors/>`_ that are
responsible for running CI builds on different targets. Development efforts
efforts where undertaken as part of ECP CI projects to introduce a new
type of executor focused on supporting HPC workflows, the batch executor.
The batch executor runs appropriate CI job scripts by submitting them to the
underlying scheduling system. Each CI job equates to a single scheduler
submission.

Currently the batch executor supports the following schedulers:

* Cobalt (using ``qsub``)
* LSF (using ``bsub -I``)
* Slurm (using ``sbatch``)

Additionally the batch executor offers support for setuid_ and advanced
Configuration_ options available to the administrator.

Configuration
-------------

Similar to how other executors in GitLab are managed, optional
configuration are available for
administrators who need to influence
the behavior of the batch executor. The only *required* configuration
to use the batch executor is ``executor = "batch"``; however, there
are other potential leavers that this documenting will highlight.

.. code-block:: toml

   [[runners]]
      executor = "batch"
      # Optional batch specific configurations
      batch_system = "cobalt"
      batch_required_args = "-A testing"
      batch_scheduler_parameters = "COBALT_PARAMETERS"
      retain_scheduler_logs = false
      scheduler_accounting_logs = "/var/logs/cobalt"

The above snippet from a `config.toml` shows all currently potential keys
relating just to batch execution. Default values are provided and detailed
in the associated sections below.

Batch System
~~~~~~~~~~~~

.. code-block:: toml

   [[runners]]
      batch_system = "cobalt"

By default the runner will attempt to find the available scheduler by
looking on the currently defined ``PATH``. In many cases this may be
undesirable, as such you may supply a matching ``batch_system`` from
the list and all interactions will use the defined interface.

+------------------+-----------+
| ``batch_system`` | Target    |
+==================+===========+
| "cobalt"         | `Cobalt`_ |
+------------------+-----------+
| "lsf"            | `LSF`_    |
+------------------+-----------+
| "slurm"          | `Slurm`_  |
+------------------+-----------+

Batch Required Specs
~~~~~~~~~~~~~~~~~~~~

**Experimental Feature**

.. code-block:: toml

   [[runners]]
      batch_required_args = "-A testing"

Allows a string of parameters to be defined that will be passed to the
scheduler in the user's batch submission request. These parameters are
simply  concatenated onto the list of existing runner defaults. There is
no additional logics at this time to support any level of parsing against
what a user has supplied.

Batch Scheduler Parameters
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: toml

   [[runners]]
      batch_scheduler_parameters = "COBALT_PARAMETERS"

A CI job is able to influence the arguments of batch submission to the
scheduler using any number of `variable types <https://docs.gitlab.com/ee/ci/variables/#priority-of-environment-variables>`_
support by GitLab. By default the runner will attempt to locate the
``SCHEDULER_PARAMETERS`` variable. Assigning a string value to the
``batch_scheduler_parameters`` key will allow the runner to look for

Take for instance the following CI job with the parameters defined above.

.. code-block:: yaml

  job:
    variables:
      COBALT_PARAMETERS: "-A account -q debug"
    script:
      - ./script.bash

In the above example if we are using the Cobalt_ scheduler the parameters
``-A account -q debug`` will be concatenated into the runner generated
``qsub`` command. Though the runner does not attempt any command cleansing
as the associated scheduler specific command will always be run under
the requesting user's UID there are several instances where a warning
message will be displayed to the user:

1. If the user attempts to provide parameters that conflicts with a runner
   defined parameter (detailed in scheduler specific documentation).

#. If the runner expected ``batch_scheduler_parameters`` is not found.

Retain Scheduler Logs
~~~~~~~~~~~~~~~~~~~~~

.. code-block:: toml

   [[runners]]
      retain_scheduler_logs = false

For the Cobalt_ and Slurm_ interfaces job output is obtained by tailing the
scheduler generated log/output files. These files are owned by and permission
protected for the local CI user. By default the files will be removed upon
completion of the job; however, by setting this to ``true`` the files will
no longer be removed.

.. note::

    Users have the ability to copy their logs files upon completion of
    a job. The setting will **not** affect this functionality, it is
    only related to the original scheduler generated files.

Scheduler Accounting Logs
~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: toml

   [[runners]]
      scheduler_accounting_logs = "/var/logs/cobalt"

Currently this is only supported for Cobalt_ and is used to supply the runner
with the appropriate directory for all scheduler accounting logs. The default
directory is `/var/log/pbs` and will be referenced by the runner at the end of
each CI job in order to determine the final ``Exit_status``. If a runner is
either unable to find the directory or appraoptie log file the CI job will
fail.

NFS Timeout
~~~~~~~~~~~

**Experimental Feature**

:sub:`Added in runner release 0.6.0`

.. code-block:: toml

   [[runners]]
      nfs_timeout = "30s"

The ``batch`` executor relies heavily upon interactions with not only the
underlying HPC scheduler but also a network file system. As such we've
added the ability to configure a ``nfs_timeout``. This is a
timeout for largest possible delay to expect from NFS servers for file data.
A `duration string <https://golang.org/pkg/time/#ParseDuration>`_ (e.g. 1m)
is required and improper formatting will lead to failing CI jobs.

In past versions of the runner we've defaults this value to a minute for both
Cobalt_ as well as Slurm_. With the addition of the configuration option
there is no longer any runner imposed timeout, instead it is now completely
optional. Future efforts regarding this configuration will seek to offer a
more comprehensive and scalable solution.

CI Job Build Stages
-------------------

When closely examining a GitLab CI job you may notice a number of distinct
shells being generated and scripts launched over the course of said job. This
behavior falls in line with the upstream GitLab runner design to breakdown
a single CI job into several stages (e.g. git sources, execute build script),
each accomplishing a specific target of
the job. In a more traditional shell executor every stage is launched in a
similar shell spawned on the host environment. In the case of the batch
executor there is a key distinction:

.. note::

    All stages within a CI job are executed as scripts under a user
    owned shell. Nothing found in these logics are ran as the Gitlab runner
    user.

1. To begin the job necessary preparations are made, source are obtained
   (`git`), and artifacts/caches are made available. Each of these stages
   within the CI job occur on the host environment of the runner.
#. If all previous stages are completed successfully the build script (a
   combination of the `before_script` and `script` sections) is submitted
   to the scheduler.
#. Finally all remaining stage, including the `after_script` again occur on
   the node where the runner is located.

Simply put only the user's `before_script` and `script` are ever submitted as
a job script to the underlying scheduler. This provides a number of benefits
to the user, chiefly that compute cycles are never wasted on potentially
minimal data management actions (e.g. relocating the runner cache). However,
you will note that the user defined `after_script` section is also run on the
host system. This is by design and allows potential users to execute potential
actions that may otherwise be impossible from a traditional compute
environment.

Scheduler Behavior
------------------

Each of the currently supported schedulers fundament attempt to fulfill the
same workflow:

* Submit the GitLab generated build script to the underlying scheduler
* Ensure successful job submission
* Monitor ongoing job status
* Pipe job `stdout`/`stderr` to the GitLab server
* Upon compellation identify final job status

As the interface between the runner and each individual scheduler differs
in it's implementation the following sections provide an overview of how
the workflow is realized.

Cobalt
~~~~~~

Jobs are submitted using `qsub <https://trac.mcs.anl.gov/projects/cobalt/wiki/qsub.1.html>`_
with both the output as well as error logs being monitored.

.. image:: files/batch_cobalt.png
    :scale: 30%

1. The runner generated build script is submitted to the scheduler using
   ``qsub``. Both stdout/stderr are managed via the ``--output`` and
   ``--error`` argument respectively. Finally all ``SCHEDULER_PARAMETERS`` are
   integrated.
#. Job state is monitored using `qstat <https://trac.mcs.anl.gov/projects/cobalt/wiki/qstat.1.html>`_
   identifying if the job is currently running on 30 second intervals.
#. Throughout the duration of the job the runner obtains the stdout/stderr
   by tailing both files and reporting back to the server.
#. Upon compellation of the job (no longer found in queue) the final exit
   status of the job is queried using the `Scheduler Accounting Logs`_
   to determine if it has passed or failed.

LSF
~~~

LSF support leverage `bsub <https://www.ibm.com/support/knowledgecenter/en/SSETD4_9.1.2/lsf_command_ref/bsub.1.html>`_
interactively by specifying an interactive job.

.. image:: files/batch_lsf.png
    :scale: 30%

1. The runner generated script is submitted to the scheduler for
   execution using ``bsub -I``. All user defined ``SCHEDULER_PARAMETERS`` are
   integrated into the request.
#. The interactive session's stdout/stderr is monitored by the runner
   and reported back to the server.
#. Due to the interactive session the exit status of the ``bsub``
   command is used to determine is a job passed or failed.

Slurm
~~~~~

The Slurm integration revolves around submitting the job scripts using
`sbatch <https://slurm.schedmd.com/sbatch.html>`_ then tailing the
subsequently generated ``--output`` log file.

.. image:: files/batch_slurm.png
    :scale: 30%

1. The runner generated script is submitted to the scheduler using
   ``sbatch``. The runner controls the schedulers ``--output`` arguments
   while all user defined ``SCHEDULER_PARAMETERS`` are also integrated
   into the request.
#. The status of the running job is polled ever 30 seconds using
   `sacct <https://slurm.schedmd.com/sacct.html>`_ until no longer running.
#. Throughout the duration of the job the runner obtains the stdout/stderr
   by tailing the file (``slurm-%j.out``). All output to this file is reported
   back to the server.
#. One a job has been completed the final state is determined again using
   sacct_.

It is important to note that the entire build script
is submitted via ``sbatch``. As such it will run entirely on the target
compute resources. Additionally, the choice to leverage ``sacct``
was implemented in release `0.5.3` in order to avoid unnecessary
locking of the Slurm database.
