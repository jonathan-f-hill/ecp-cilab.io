Server Administration
=====================

.. _downscoping: ../jacamar/auth.html#downscoping-mechanisms

Security Considerations
-----------------------

The following should be taken into account when configuring the GitLab server.

Impersonation Through CI Jobs
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. warning::

    If users can change their username any related downscoping_ operation
    is fundamentally insecure and should not be used.

When attempting to downscope to a local user Jacamar will leverage that user's
login name as provided by a verified JSON web token, signed by the GitLab
server. This opens the potential for a GitLab user to impersonate another user
on system resources via CI jobs if **they are capable** of changing their
username on the server.

.. image:: jacamar/files/change_username.png

By default (and with some LDAP_/SAML_ configurations) it is possible
for a user to directly influence their username. If required this can
be managed via the server configuration:

.. code-block:: rb

    gitlab_rails['gitlab_username_changing_enabled'] = false

LDAP
~~~~

Official
`LDAP documentation <https://docs.gitlab.com/ee/administration/auth/ldap.html#security>`_
provides a clear recommendation, "against using LDAP integration if your LDAP
users are allowed to change their *mail*, *email* or *userPrincipalName*
attribute on the LDAP server or share email addresses." This is
of even a higher important when attempting to manage a runner capable of
downscoping_ to a local account.

SAML
~~~~

By default, GitLab uses the NameID returned by the SAML response for the UID.
If your organization's NameID format is transient, the name identifier changes
for each login, thus making it unsuitable to be used as a user identifier.

To address this, the ``uid_attribute`` option should be used, which allows
a SAML 2.0 attribute to be used as the user identifier in place of NameID.

See the docs here: https://docs.gitlab.com/ee/integration/saml.html#uid_attribute

**Account Takeover**

* If users are able to change their own email addresses, auto-linking
  SAML users should be set to false in the configuration.

  - .. code-block:: rb

       gitlab_rails['omniauth_auto_link_saml_user'] = false

  - Otherwise a user can potentially take over another user's account
    on the GitLab instance simply by changing their email address.


* GitLab generates usernames based on email addresses. There is not an option
  to configure GitLab to use a different attribute to generate the username.

  - If a user has not yet created an account, another user can obtain
    the username by changing their email address to match that of the
    username of the account they wish to compromise.
