# Each job must be assigned a stage and the stages can be defined to meet the 
# project requirements. The naming of the stages can be entirely driven by the project.
stages:
  - build
  - test

# Variables are a core concept of GitLab CI as each job within a pipeline is 
# provided a prepared build environment that incorporates predefined variables
# along with user-defined ones.
variables:
  APPLICATION_NAME: "hello"
  # A service user must be specified for ALCF resources.
  ANL_THETA_PROJECT_SERVICE_USER: "<service>"

# 'build-Hello' is this specific job with a name we have defined (could be anything)
# This job will be run in the build stage ('stage: build'), and only
# after all job's in a stage have been completed will the next stage in the 
# the pipeline (complete .gitlab-ci.yml) will be run. Please see GitLab's
# official documentation for more info: https://docs.gitlab.com/ee/ci/pipelines.html
build-hello:
  stage: build
  # Tags are used to select specific Runners from those available to the project.
  # The admin who deploys / registers the runners defines these tags and by
  # specifying them we can ensure our jobs are deployed on the correct system.
  # You can identify the details of the runners available to the project by:
  # Settings > CI/CD > Runners
  tags: [shell, ecp-theta, anl]
  # Script is the only required keyword for a job. Anything you are able to do in a job
  # on the target systems with Bash can be included here. It is important to
  # remember everything will be run as a Bash script, there is not other option available.
  script:
    - module load gcc
    - gcc main.c -o $APPLICATION_NAME
  # The 'artifacts' keyword is used to specify a list of files and/or 
  # directories to be uploaded to the GitLab server upon completion of the job.
  # These can be either downloaded manually or made available to other jobs in the
  # pipeline. In this example we are capturing a binary that we are building.
  # Source: https://docs.gitlab.com/ee/ci/yaml/#artifacts
  artifacts:
    # We are leveraging a predefined GitLab environment variable to give the 
    # artifacts (upload to GitLab server) a name.
    # Source: https://docs.gitlab.com/ee/ci/variables/#predefined-environment-variables
    name: "Binary-$CI_JOB_NAME"
    paths:
      - $APPLICATION_NAME

# The next two jobs ('test-Hello' and 'test-General') are part of the test 
# stage. They will be submitted to the appropriate runners at the same time
# however, they don't necessarily run in parallel.
test-hello:
  stage: test
  # This example uses the same runner for each job; however, it is possible
  # to utilize different runners for each job through proper usage of tags.
  tags: [shell]
  script:
    - ls -l
    - ./$APPLICATION_NAME
  # The `dependencies` are used to define from which job artifacts should be
  # downloaded from. By default ALL artifacts from previous stages will be 
  # downloaded. Source: https://docs.gitlab.com/ee/ci/yaml/#dependencies
  dependencies:
    - build-hello

test-general:
  stage: test
  tags: [shell]
  # We can influence any variables in a number of different ways. In this example, we are defining
  # job-level variables that will be used as opposed to the project-level (defined at the top).
  # Source: https://docs.gitlab.com/ee/ci/variables/#priority-of-variables
  variables:
    APPLICATION_NAME: "job"
  script:
    - ls -l
    - pwd
    - env | grep APPLICATION_NAME
  # Due the the default behavior or artifacts it is possible to avoid downloading
  # any by specifying an empty list for dependencies
  dependencies: []
